FROM alpine:3
ENV DISCORD_TOKEN=

RUN apk update
RUN apk add git
RUN apk add nodejs
RUN addgroup -S example --gid 1000 && adduser --uid 1000 -S example -G example
RUN ln -s /home/example/example-bot /server
ADD init.sh /
RUN chown 1000:1000 /init.sh

USER example
WORKDIR /home/example
RUN git clone https://gitlab.com/discord5/example-bot.git
WORKDIR /server

ENTRYPOINT [ "/init.sh" ]

