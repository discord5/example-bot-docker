#!/bin/sh

cd /server

# Remove this following line to keep the image static 
git pull

echo DISCORD_TOKEN=$DISCORD_TOKEN > .env

node bot.js > /dev/null

